# ETW-via-csharp
A link-collection, knowledge-base and examples to implement ETW tracing/consuming using C#

## What is ETW?
- Event Tracing for Windows (ETW) provides application programmers the ability to start and stop event tracing sessions, instrument an application to provide trace events, and consume trace events.

- Event Tracing for Windows (ETW) is an efficient kernel-level tracing facility that lets you log kernel or application-defined events to a log file. You can consume the events in real time or from a log file and use them to debug an application or to determine where performance issues are occurring in the application.

## ETW Glossary
- **ETW provider**: (native concept) a component capable of firing ETW events.
- **ETW manifest**: (native concept) metadata describing an ETW provider and the detailed information on the ETW events it might fire.
- **ETW keywords**: (native concept) bit-flags that can be associated with events to create event “categories”.
- **ETW tasks**: (native concept) small integers that can be associated with events to define “task-oriented” groupings. Generally used in conjunction with opcodes.
- **ETW opcodes**: (native concept) small integers that identify an operation within a Task. The value of Opcodes is that there are some well-known ones like ‘Start’ and ‘Stop’ which allow tools to operate on events in a generic way.
- **Utility Event Source**: a user-defined abstract class derived from the EventSource type. It may not define any ETW-specific elements (keywords, tasks, opcodes, channels, or events). It may define methods that may be used by derived event source types.
- **Event Source**: a user-defined sealed class derived from the EventSource type or from a user-defined Utility Event Source.
- **ETW Event Method**: a method defined in an Event Source that fires an ETW event. Any instance method defined in an Event Source class not marked with the `[NonEvent]` attribute will be considered an ETW Event Method. Virtual or void-returning methods are included, starting with the NuGet version of EventSource, only if they are marked with the `[Event]` attribute.
- **ETW Transfer Event Method**: an ETW event method that marks a relation between the current activity and a related activity (supported starting with v4.5.1 and the NuGet package).

## How to register to Windows Event Log
- [wevtutil](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/wevtutil)
